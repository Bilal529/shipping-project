# Generated by Django 2.2.3 on 2020-07-03 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_auto_20200630_2246'),
    ]

    operations = [
        migrations.AddField(
            model_name='containerpricing',
            name='model_link',
            field=models.CharField(blank=True, default='', max_length=10, null=True, verbose_name='model_link'),
        ),
    ]
