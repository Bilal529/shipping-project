# Generated by Django 2.2.3 on 2020-06-30 17:14

from django.db import migrations, models
import localflavor.us.models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0005_auto_20200614_2138'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(default='', max_length=1000, verbose_name='Address')),
                ('city', models.CharField(default='', max_length=60, verbose_name='City')),
                ('state', models.CharField(default='', max_length=60, verbose_name='State')),
                ('postal', localflavor.us.models.USZipCodeField(default='', max_length=10, verbose_name='Postal')),
                ('country', models.CharField(default='', max_length=60, verbose_name='Country')),
                ('delivery_date', models.DateField(default=None)),
            ],
            options={
                'verbose_name': 'Delivery Info',
                'verbose_name_plural': 'Delivery Info',
            },
        ),
        migrations.RemoveField(
            model_name='cartorder',
            name='delivery_date',
        ),
        migrations.AddField(
            model_name='cartorder',
            name='furnishing_option',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='furnishing_option'),
        ),
        migrations.AlterField(
            model_name='order',
            name='email',
            field=models.EmailField(blank=True, help_text='Optional', max_length=100, null=True, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='order',
            name='when_to_order',
            field=models.CharField(blank=True, choices=[('urgent', 'Urgent(within 30 days)'), ('other', 'Other')], default=None, max_length=50, null=True, verbose_name='When are you looking to order?'),
        ),
    ]
