# Generated by Django 2.2.3 on 2020-07-03 10:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_modelimages_pdf_file_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userpreferences',
            name='user_obj',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.SpecUser'),
        ),
    ]
