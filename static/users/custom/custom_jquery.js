(function($) { // Begin jQuery
    $(function() { // DOM ready
        // // If a link has a dropdown, add sub menu toggle.
        // $('nav ul li a:not(:only-child)').click(function(e) {
        //     $(this).siblings('.nav-dropdown').toggle();
        //     // Close one dropdown when selecting another
        //     $('.nav-dropdown').not($(this).siblings()).hide();
        //     e.stopPropagation();
        // });
        // // Clicking away from dropdown will remove the dropdown class
        // $('html').click(function() {
        //     $('.nav-dropdown').hide();
        // });
        // // Toggle open and close nav styles on click
        // $('#nav-toggle').click(function() {
        //     $('nav ul').slideToggle();
        // });

        // // Hamburger to X toggle
        // $('.navbar-toggler').on('click', function() {
        //     $('#overlay').slideToggle();
        //     $('.color_15').css({"color":"black"});
        // }
        function fixed_nav(){
            var nav = $("#nav-header");
            try{
                var sticky = nav[0].offsetTop;
                if (window.pageYOffset > sticky && window.location.pathname != "/register/" && window.screen.width > 992) {
                nav.addClass("fixed-top");
                } else {
                  nav.removeClass("fixed-top");
                }
            }catch (e) {
                console.log("error",e)
            }
        }
        fixed_nav()
        window.onscroll = function() {fixed_nav()};


        //remove blank options
        $("ul[id=id_septic_infrastructure] > li:first").remove();
        $("ul[id=id_installation_septic_infrastructure] > li:first").remove();
        $("ul[id=id_3-letter_of_credit] > li:first").remove();
        $("ul[id=id_3-line_of_credit] > li:first").remove();
        $("ul[id=id_2-learn_about_electric_drive] > li:first").remove();
        $("ul[id=id_learn_about_electric_drive] > li:first").remove();
        $("ul[id=id_1-septic_infrastructure] > li:first").remove();
        $("ul[id=id_1-installation_septic_infrastructure] > li:first").remove();


        // add dashes to phone number
        $('#id_1-phone_number').keyup(function() {
            $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{3})/, '$1-$2-$3'))
        });

        $('#id_phone_number_8').keyup(function() {
            $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'))
        });

        
        // letter_of_credit show/hide
        letter_of_credit_input()
        $("#id_3-letter_of_credit").on("click",function() {
            letter_of_credit_input()
        });

        // line_of_credit show/hide
        line_of_credit_input()
        $("#id_3-line_of_credit").on("click",function() {
            line_of_credit_input()
        });


        //When are u looking to order
        $("#id_1-when_to_order_0").click(function() {
            $('#id_1-other_when_to_order').removeAttr('required');
            $('#id_1-other_when_to_order').val('');
            $("#id_12").hide();
        });
        if ($("#id_1-when_to_order_1").is(":checked")){
            $("#id_12").show();
            $('#id_1-other_when_to_order').attr('required', 'required');
        }

        $("#id_1-when_to_order_1").click(function() {
            $("#id_12").show();
            $('#id_1-other_when_to_order').attr('required', 'required');
        });
        show_hide_fields()
        // other1_in registration_step_3
        $("#id_2-type_of_development_10").click(function() {
            show_hide_fields()
        });
        $("#id_type_of_development_10").click(function() {
            show_hide_fields()
        });

        // other2_climate_area
        function other2_climate_area(){
            if ($('#id_1-type_of_climate_area_6').is(":checked")) {
                $('#id_1-other_type_of_climate_area').attr('required', 'required');
                $('#div_id_1-other_type_of_climate_area').show();
                }
            else {
                $('#id_1-other_type_of_climate_area').removeAttr('required');
                $('#id_1-other_type_of_climate_area').val('');
                $("#div_id_1-other_type_of_climate_area").hide();
            }
            if ($('#id_type_of_climate_area_6').is(":checked")) {
                $('#id_other_type_of_climate_area').attr('required', 'required');
                $('#div_id_other_type_of_climate_area').show();
                }
            else {
                $('#id_other_type_of_climate_area').removeAttr('required');
                $('#id_other_type_of_climate_area').val('');
                $("#div_id_other_type_of_climate_area").hide();
            }
        }
        $("#id_1-type_of_climate_area_6").click(function() {
            other2_climate_area()
        });
        other2_climate_area()

        //admin-check Time access show/hide
        $("input[name=selector]").click(function() {
            access_time_admin()
        });
        access_time_admin()

        //register.html
        $("#id_1-user_type").click(function() {
            register_Validation()
        });
        register_Validation()

        var $on = 'section';
        $($on).css({
            'background': 'none',
            'border': 'none',
            'box-shadow': 'none'
        });
// $("ul[id=id_1-letter_of_credit] > li:first").remove();
        //add select all box
        function create_select_box(id){
            var select_box = '<li><label for="'+id+'"><input type="checkbox"' +
            ' name="select_all"' +
            ' value="select all" id="'+id+'">\n' +
            ' Select All<span class="checkmark"></span></label>\n' +
            '</li>'
            return select_box
        }

        $("ul[id=id_2-type_of_smart_home]").prepend(create_select_box("type_of_smart_home_select_all"))
        $("ul[id=id_type_of_smart_home]").prepend(create_select_box("type_of_smart_home_select_all"))
        $("ul[id=id_2-type_of_electric_vehicle_function]").prepend(create_select_box("type_of_electric_vehicle_select_all"))
        $("ul[id=id_type_of_electric_vehicle_function]").prepend(create_select_box("type_of_electric_vehicle_select_all"))


        //select all
        $("#type_of_smart_home_select_all").click(function() {
            $("#id_2-type_of_smart_home_0").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_smart_home_1").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_smart_home_2").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_smart_home_3").prop('checked', $(this).prop('checked'));

            $("#id_type_of_smart_home_0").prop('checked', $(this).prop('checked'));
            $("#id_type_of_smart_home_1").prop('checked', $(this).prop('checked'));
            $("#id_type_of_smart_home_2").prop('checked', $(this).prop('checked'));
            $("#id_type_of_smart_home_3").prop('checked', $(this).prop('checked'));
        });

         $("#type_of_electric_vehicle_select_all").click(function() {
            $("#id_2-type_of_electric_vehicle_function_0").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_electric_vehicle_function_1").prop('checked', $(this).prop('checked'));

            $("#id_type_of_electric_vehicle_function_0").prop('checked', $(this).prop('checked'));
            $("#id_type_of_electric_vehicle_function_1").prop('checked', $(this).prop('checked'));
        });

        $("#id_2-type_of_development_0").click(function() {
            $("#id_2-type_of_development_1").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_2").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_3").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_4").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_5").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_6").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_7").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_8").prop('checked', $(this).prop('checked'));
            $("#id_2-type_of_development_9").prop('checked', $(this).prop('checked'));
        });
        $("#id_type_of_development_0").click(function() {
            $("#id_type_of_development_1").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_2").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_3").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_4").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_5").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_6").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_7").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_8").prop('checked', $(this).prop('checked'));
            $("#id_type_of_development_9").prop('checked', $(this).prop('checked'));
        });

        //select all
        $("#id_1-type_of_climate_area_0").click(function() {
            $("#id_1-type_of_climate_area_1").prop('checked', $(this).prop('checked'));
            $("#id_1-type_of_climate_area_2").prop('checked', $(this).prop('checked'));
            $("#id_1-type_of_climate_area_3").prop('checked', $(this).prop('checked'));
            $("#id_1-type_of_climate_area_4").prop('checked', $(this).prop('checked'));
            $("#id_1-type_of_climate_area_5").prop('checked', $(this).prop('checked'));
        });

        $('#id_1-how_much_letter_of_credit').inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': true, 'prefix': '$'})

        $('#id_1-how_much_line_of_credit').inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': true, 'prefix': '$' })

        $("input[name=price]").inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': true, 'prefix': '$', 'align': 'left' })

        let path = window.location.pathname;
        if (path == '/order-form/') {
            $('[href="/order-form/"]').css({'color':"white"});
        }
        else if (path == '/login/') {
            $('[href*="/login"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        } else if (path == '/buyer/applications') {
            $('[href="/buyer/applications"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        } 
        else if (path == '/floor-plan') {
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/floor-plan"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path == '/assembling/') {
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/assembling/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path == '/quotation/') {
            $('[href*="/quotation/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path == '/view/quotation/') {
            $('[href*="/view/quotation/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path == '/register/') {
            $('[href*="/register"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path == '/dealer/') {
            $('[href*="/dealer/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path.startsWith('/models/1S-2W') ) {
            // $('.models').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/models/1S-2W"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.models').attr('style', 'color: #5ca6cf!important; font-weight: bold');

        }
         else if (path.startsWith('/models/2S-3W') ) {
            $('.models').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/models/2S-3W"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else if (path.startsWith('/models/3S-2W') ) {
            $('.models').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/models/3S-2W"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path == '/electric-cars/') {
            $('[href="/electric-cars/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.electric_car').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path == '/electric-cars/interior') {
            $('.electric_car').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/electric-cars/interior"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path == '/electric-cars/exterior') {
            $('.electric_car').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('[href*="/electric-cars/exterior"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/view/structural-drawings') != -1) {
            $('[href="/view/structural-drawings"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/view/architectural-drawings') != -1) {
            $('[href="/view/architectural-drawings"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path == '/concept') {
            $('[href*="/concept"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path == '/amenities') {
            $('[href*="/amenities"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/view/reportsap') != -1) {
            $('[href="/view/reportsap"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            $('.about').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/contactus') != -1) {
            $('[href="/contactus"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
            
        }
        else if (path.indexOf('/my/orders') != -1) {
            $('[href="/my/orders"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/preferences/') != -1) {
            $('[href="/preferences/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/update/profile') != -1) {
            $('[href="/update/profile"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
        else if (path.indexOf('/logout') != -1) {
            $('[href="/logout"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }
         else {
            $('[href="/"]').attr('style', 'color: #5ca6cf!important; font-weight: bold');
        }

    }); // end DOM ready

    /// show/hide access time 
    function access_time_admin() {
        if ($('#f-option').is(":checked")) {
            $('#time_access_input').attr('required', 'required');
            $("#access_time_div").show();
        } else {
            $('#time_access_input').removeAttr('required');
            $("#access_time_div").hide();
        }
    }
    change_class()
    function change_class(){
          var isNarrow = $(window).width() < 1200;
          $(".change_class")
              .toggleClass("col-md-3", !isNarrow)
              .toggleClass("col-md-5 m-auto pt-3 pb-3", isNarrow);
    }
    $(window).resize(function() {
        change_class()
    });

    /// signup/register.html validation
    function register_Validation() {
        if ($('#id_1-user_type').val() == "homeowner") {
            $("#div_id_dealer_no").show();
            $("#div_id_company_name").hide();
            $("#id_1-company_name").val('');
            $("#div_id_title").hide();
            $("#id_1-title").val('');

        } else if ($('#id_1-user_type').val() == "dealer") {
            $("#div_id_company_name").hide();
            $("#id_1-company_name").val('');
            $("#div_id_title").hide();
            $("#id_1-title").val('');
            $("#div_id_dealer_no").hide();
            $("#id_1-dealer_no").val('');

        } else if ($('#id_1-user_type').val() == "Municipality/Government Official") {
            $("#div_id_company_name").hide();
            $("#id_1-company_name").val('');
            $("#div_id_title").hide();
            $("#id_1-title").val('');
            $("#div_id_dealer_no").hide();
            $("#id_1-dealer_no").val('');
        } else if ($('#id_1-user_type').val() == "developer" || $('#id_1-user_type').val() == "lender" || $('#id_1-user_type').val() == "dealer" ) {
            $("#div_id_company_name").show();
            $("#div_id_title").show();
            $("#div_id_dealer_no").hide();
            $("#id_1-dealer_no").val('');
        } else if ($('#id_1-user_type').val() == "vendor") {
            $("#div_id_company_name").show();
            $("#div_id_title").hide();
            $("#id_1-title").val('');
            $("#div_id_dealer_no").hide();
            $("#id_1-dealer_no").val('');
        }
    }

    // form submissions
    $("#print_name").on("focusout",function() {
        $("#buyer_name").text($("#print_name").val()) 
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var output = d.getFullYear() + '/' +
            ((''+month).length<2 ? '0' : '') + month + '/' +
            ((''+day).length<2 ? '0' : '') + day;
        $(".date_today").text(output) 
    });

    // on form submission
    $("#order_form").submit(function (e) {
            // $("#buyer_name").text($("#print_name").val()) 
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var output = d.getFullYear() + '/' +
                ((''+month).length<2 ? '0' : '') + month + '/' +
                ((''+day).length<2 ? '0' : '') + day;
            $(".date_today").text(output) 
            
        });
    // on form submission
    $("#id_2-accept_1").click(function()
        {
            Webcam.snap( function(image) {
                $("#image-input_id").val(image);
            });
        });
    
    // $("#multi-step-continue").click(function()
    //     {
    //         if($("#image-input_id").val() == ""){
    //             Webcam.snap( function(image) {
    //                 $("#image-input_id").val(image);
    //             });
    //         }
    // });

    $("#capture").click(function()
        {
        capture_image()
        $("#capture").text('ReTake')
    });
    if($("#image-input_id").val() != ""){
        const image = $("#image-input_id").val();
        var img = '<img class="img-fluid" src="'+image+'"/>'
        $("#capture_image").html(img)
    }


    $(".close").click(function(){
        $(".capture_image_div").toggleClass('open');
    });

    checkbox_checked()
    $(":checkbox").on("change",function(){
        checkbox_checked()
    });

    radio_checked()
    $(":radio").on("change",function(){
        radio_checked()
    });

    $("#form_submission").click(function() {
        form_submissions()
    });



})(jQuery); // end jQuery

function radio_checked(){
    $(":radio").each(function(){
        if($(this).is(":checked")){
            $(this).parent().addClass("checkbox_checked"); 
        }else{
            $(this).parent().removeClass("checkbox_checked");  
        }
    });
}

function checkbox_checked(){
    $(":checkbox").each(function(){
        if($(this).is(":checked")){
            $(this).parent().addClass("checkbox_checked"); 
        }else{
            $(this).parent().removeClass("checkbox_checked");  
        }
    });
}

function format(){
    $('#id_1-how_much_letter_of_credit').inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': true, 'prefix': '$' })
}
function total(){
    $('#total').inputmask({ 'alias': 'numeric', 'groupSeparator': ',', 'digits': 2, 'digitsOptional': true, 'prefix': '$' })
}

function ajax() {
    document.getElementById("price_span").innerHTML= "$ " + $('#myselect option:selected').val().toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"); 
}

function form_submissions(form_id){
    console.log("form",form_id)
    var frm = $(`#${form_id}`);
        $.ajax({
            type: "POST",
            url: "/add-order/",
            data: frm.serialize(),
            dataType: "json",
            success: function (data) {
                var parts = data.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                parts.join(".")
                $("#total,#grand-total").html("$ " + parts.join("."));
            },
            error: function(data) {
                console.log("error",data)
                // $("#MESSAGE-DIV").html("Something went wrong!");
            }
        });
        return false;
    }
// function form_submissions(){
//     var frm = $("#order_form");
//         $.ajax({
//             type: "GET",
//             url: "/add-order/",
//             data: frm.serialize(),
//             dataType: "json",
//             success: function (data) {
//                 var parts = data.toString().split(".");
//                 parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//                 parts.join(".")
//                 $("#total,#grand-total").html("$ " + parts.join("."));
//             },
//             error: function(data) {
//                 // console.log("error",data)
//                 // $("#MESSAGE-DIV").html("Something went wrong!");
//             }
//         });
//         return false;
// }

function show_hide_fields(){
    if ($('#id_2-type_of_development_10').is(":checked")) {
        $("#div_id_2-other_type_of_development").show();
        $('#id_2-other_type_of_development').attr('required', 'required');
    } else {
        $('#id_2-other_type_of_development').removeAttr('required');
        $('#id_2-other_type_of_development').val('');
        $("#div_id_2-other_type_of_development").hide();
    }
    if ($('#id_type_of_development_10').is(":checked")) {
        $("#div_id_other_type_of_development").show();
        $('#id_other_type_of_development').attr('required', 'required');
    } else {
        $('#id_other_type_of_development').removeAttr('required');
        $('#id_other_type_of_development').val('');
        $("#div_id_other_type_of_development").hide();
    }
}
function myFunction(){
    $('#b-color').css("color","black");
}   
function letter_of_credit_input(){
    if ($("#id_3-letter_of_credit_1").is(":checked")){
        $("#div_id_3-how_much_letter_of_credit").show();
        $('#id_3-how_much_letter_of_credit').attr('required', 'required');
    }
    else{
        $('#id_3-how_much_letter_of_credit').removeAttr('required');
        $('#id_3-how_much_letter_of_credit').val('');
        $("#div_id_3-how_much_letter_of_credit").hide();
    }
}
function line_of_credit_input(){
    if ($('#id_3-line_of_credit_1').is(":checked")) {
        $("#div_id_3-how_much_line_of_credit").show();
        $('#id_3-how_much_line_of_credit').attr('required', 'required');
    }
    else{
        $('#id_3-how_much_line_of_credit').removeAttr('required');
        $('#id_3-how_much_line_of_credit').val('');
        $("#div_id_3-how_much_line_of_credit").hide();
    }
}

function capture_image(){
        Webcam.snap( function(image) {
        $("#image-input_id").val(image);
        // display results in page
        var img = '<img class="img-fluid" src="'+image+'"/>'
        $("#capture_image").html(img)
        });
}